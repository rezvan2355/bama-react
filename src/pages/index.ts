import AboutPage from './About';
import HomePage from './Home';
import NoMatch from './NoMatch';

// const pages = {
//   AboutPage,
//   HomePage,
// };

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  AboutPage,
  HomePage,
  NoMatch,
};
